import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import keras
from sklearn.model_selection import train_test_split
from keras.layers import Dense
from keras.models import Sequential
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
import keras.regularizers
from sklearn.covariance import EllipticEnvelope
from sklearn import svm

def read_data(file_name, train=True):
    """Read data from csv
    
    :param file_name:str path to file
    :param train:boolean decide if data are from training set
    :return: pca_reduct contains input data
            target contains array with output for each point
    """

    df = pd.read_csv(file_name, index_col=0)
    pca_reduct, target = pca_reduction(df, train)
    check_correlation(df)
    return pca_reduct, target, np.shape(pca_reduct)[1]


def pca_reduction(data_frame, train=True):
    """This function is reducing data's diemension into 7 dimension using PCA algorythm.
        there is possibility to see data in some dimensions
    
    :param data_frame:cointans data read from csv file 
    :param train:boolean decide if data are from training set 
    :return: 
    """
    if train:
        target = np.array(data_frame['y'])
    else:
        target = []

    input_data = data_frame.ix[:, :-1]
    pca_2c = PCA(n_components=7)
    scaler2 = StandardScaler()
    X_pca_2c = scaler2.fit_transform(input_data)
    X_pca_2c = pca_2c.fit_transform(X_pca_2c)
    # X_pca_2c, target = find_outliners(X_pca_2c, target)
    print('Energy reduction' + str(pca_2c.explained_variance_ratio_.sum()))
    return X_pca_2c, target


def plot_after_filtering(X_pca_2c, target):
    fig = plt.figure()
    for i in range(4):
        ax = fig.add_subplot(221 + i)
        ax.scatter(X_pca_2c[:, 4], X_pca_2c[:, i], c=target,
                        alpha=0.8, s=30, marker='o', edgecolors='white')
    fig.suptitle('Data after filtering and reduction')

    plt.show()

    return X_pca_2c, target


def find_outliners(data, target):
    robust_covariance_est = EllipticEnvelope(contamination=0.01).fit(data)
    detection = robust_covariance_est.predict(data)
    inliers = np.where(detection == 1)

    return data[inliers], target[inliers]


def find_outliners_by_svm(data, target, train=True):
    """This function uses SVM clasificator to find outliners in data. 
    Points from second class are not removed because of their small number
    
    :param data:  data read from csv file contains input
    :param target:  data read from csv file contains output
    :param train: 
    :return:
    """
    outliers_fraction = 0.02
    nu_estimate = 0.95 * outliers_fraction + 0.01
    machine_learning = svm.OneClassSVM(kernel="rbf", gamma=1.0 / len(data), degree=3, nu=nu_estimate)

    machine_learning.fit(data)
    detection = machine_learning.predict(data)
    outliers = np.where(detection == -1)
    inliers = np.where(detection == 1)

    if train:
        fig = plt.figure()
        for i in range(4):
            ax = fig.add_subplot(221 + i)
            in_points = ax.scatter(data[inliers, 4], data[inliers, i],
                               c=target[inliers], alpha=0.8, s=30, marker='o', edgecolors='white')
            out_points = ax.scatter(data[outliers, 4], data[outliers, i],
                                c=target[outliers], alpha=0.8, s=30, marker='o', edgecolors='red')
        fig.suptitle('Filtering data')
        fig.legend((in_points, out_points), ('inliers', 'outliers'),
                   scatterpoints=1)
        third_class = np.where(target == 2)
        third_class = third_class[0]
        inliers = inliers[0]
        inliers = np.concatenate((third_class,inliers))
        inliers = np.unique(inliers)
        target = target[inliers]

    return data[inliers], target


def check_correlation(data_frame):
    """Show correlation between each dimension
    
    :param data_frame: input data
    :return: 
    """
    cov_data = np.corrcoef(data_frame.T)
    img = plt.matshow(cov_data, cmap=plt.cm.rainbow)
    plt.colorbar(img, ticks=[-1, 0, 1], fraction=0.045)
    plt.title('Correlation beetwen each dimension')
    plt.show()


def train_model(X, Y, input_dim, output_dim, epoch):
    """Create and train neural network clasifier 
    
    :param X: 
    :param Y: 
    :param input_dim: 
    :param output_dim: 
    :param epoch: 
    :return: 
    """
    X, x_test, Y, y_test = train_test_split(X, Y, test_size=0.25, random_state=0)
    Y = keras.utils.to_categorical(Y, output_dim)
    Y_test = keras.utils.to_categorical(y_test, output_dim)

    model = Sequential()
    model.add(Dense(35, input_shape=(input_dim, ), activation='relu', activity_regularizer=keras.regularizers.l2(0.002)))
    model.add(Dense(25, activation='relu', activity_regularizer=keras.regularizers.l2(0.002)))

    model.add(Dense(15, activation='relu'))
    model.add(Dense(5, activation='relu'))

    model.add(Dense(output_dim, activation='softmax'))
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
    model.fit(X, Y, epochs=epoch, batch_size=5)
    score = model.evaluate(x_test, Y_test, verbose=0)
    Y_pred = model.predict(x_test)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    Y_pred = np.argmax(Y_pred, axis=1)
    check_metrics(Y_pred, y_test)
    return model


def check_metrics(Y_test, Y_pred):
    """Plot confusion matrix
    
    :param Y_test: 
    :param Y_pred: 
    :return: 
    """
    cm = confusion_matrix(Y_test, Y_pred)
    img = plt.matshow(cm, cmap=plt.cm.autumn)

    plt.colorbar(img, fraction=0.045)
    for x in range(cm.shape[0]):
        for y in range(cm.shape[1]):
            plt.text(x, y, "%0.2f" % cm[x, y],
                size=12, color='black', ha="center", va="center")
    plt.title('Confusion matrix')

    plt.show()

def predict_data(model, input, file_name):
    output = model.predict(input)
    df = pd.read_csv(file_name, index_col=0)
    df['y'] = pd.Series(np.argmax(output, axis=1), index=df.index)
    df.to_csv( path_or_buf='recruitment_task_df_test.csv')


if __name__ == "__main__":

    name_file = "recruitment_task_df_train.csv"
    input_data, output, input_dim = read_data(name_file)
    input_data, output = find_outliners_by_svm(input_data, output)
    plot_after_filtering(input_data, output)
    model = train_model(np.array(input_data), np.array(output), input_dim, 3, 25)
    test_name_file = 'recruitment_task_df_test.csv'
    input_data, _, input_dim = read_data(test_name_file, train=False)
    predict_data(model, input_data, test_name_file)

